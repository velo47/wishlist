# Ønskeseddel. Links er til hjemmesider, der ikke nødvendigvis sælger produktet eller er det bedste sted at købe. Pricerunner etc. kan bruges til at finde bedste sted at købe.
## Tøj
#### Barbour Classic Bedale oliven, str. UK36 - EU46
https://www.careofcarl.dk/dk/artiklar/barbour-lifestyle-classic-bedale-jacket-olive.html
## Udstyr
#### Montblanc Meisterstück Classique Around the World in 80 Days Ballpoint Kuglepen
https://www.goldentime.dk/mont-blanc-meisterstack-classique-around-the-world-in-80-days-ballpoint-pen-126347/
## Nintendo Switch spil
#### Super Mario 3D All-Stars, Nintendo Switch
https://www.nintendopusheren.dk/vare/super-mario-3d-all-stars-2/
#### Luigi's Mansion 3, Nintendo Switch
https://www.nintendopusheren.dk/vare/luigis-mansion-3-2/
#### Hollow Knight, Nintendo Switch
https://www.nintendopusheren.dk/vare/hollow-knight-5/
#### Ori: The Collection, Nintendo Switch
https://www.nintendopusheren.dk/vare/switch-nyforudbestilling-3/
#### Shovel Knight: Treasure Trove, Nintendo Switch
https://www.nintendopusheren.dk/vare/shovel-knight-treasure-trove-2/
#### Metroid Dread, Nintendo Switch
https://www.nintendopusheren.dk/vare/metroid-dreadswitch-ny/


[//]: # (Philips Hue A60 E27 trådløst startsæt til hvidt og farvet stemningslys)
[//]: # (https://www.apple.com/dk/shop/product/HNTC2ZM/A/philips-hue-startsæt-til-hvid-og-farvet-belysning)
